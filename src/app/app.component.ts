import { Component } from '@angular/core';
import {Location} from '@angular/common';
import { Login } from './login';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'vilz';
  constructor(private _location: Location) 
  {}



  backClicked() { console.log("here clicked");
    this._location.back();
  }
  
}
