import { TestBed } from '@angular/core/testing';

import { SubmitSignupService } from './submit-signup.service';

describe('SubmitSignupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubmitSignupService = TestBed.get(SubmitSignupService);
    expect(service).toBeTruthy();
  });
});
