import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import {SubmitLoginService} from '../submit-login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _submitLoginService: SubmitLoginService){}
  userModel = new Login('','');

  ngOnInit() {
  }
  onSubmit(){
    console.log(this.userModel);
    this._submitLoginService.checkLogin(this.userModel)
    .subscribe(
        (data)=>{
          alert("Logged in successfully");
          window.location.href="/dashboard";
      },
        error=>console.log("error")
    )
  }
}
