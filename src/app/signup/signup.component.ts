import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { Signup } from '../signup';
import {SubmitSignupService} from '../submit-signup.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  //constructor() { }
  constructor(private location: Location,private _submitSignupService: SubmitSignupService) 
  {}
  userModel = new Signup('','','','','');
  ngOnInit() {
  }

  backClicked() {
    this.location.back();
  }
  submitSignup(){
    console.log(this.userModel);
    if(this.userModel.password==this.userModel.password2){
        this._submitSignupService.checkSubmit(this.userModel)
        .subscribe(
          (data)=>{
            alert("Registered successfully");
            window.location.href="/login";
        },
          error=>alert("Something went wrong!")
        )
      }else{
        alert("Givent passwords are not matching");
      }
    }
    
}
