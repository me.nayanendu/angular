import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Login } from './login';

@Injectable({
  providedIn: 'root'
})
export class SubmitLoginService {

  _url= 'https://digisoftech.com/project/dummyApi/checkLogin.php';
  constructor(
    private _http: HttpClient
  ) { }

  checkLogin(login: Login){ 
    
    return this._http.post<any>(this._url, login);
  }
}
