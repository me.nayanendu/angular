import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forget-pass',
  templateUrl: './forget-pass.component.html',
  styleUrls: ['./forget-pass.component.css']
})
export class ForgetPassComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  submitForget(){
    alert("An password reset link has been sent to your email address.")
  }
}
