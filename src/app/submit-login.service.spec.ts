import { TestBed } from '@angular/core/testing';

import { SubmitLoginService } from './submit-login.service';

describe('SubmitLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubmitLoginService = TestBed.get(SubmitLoginService);
    expect(service).toBeTruthy();
  });
});
