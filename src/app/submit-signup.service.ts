import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Signup } from './signup';

@Injectable({
  providedIn: 'root'
})
export class SubmitSignupService {
  _url= 'https://digisoftech.com/project/dummyApi/checkSignup.php';
  constructor(
    private _http: HttpClient
  ) { }

  checkSubmit(signup: Signup){
   return this._http.post<any>(this._url,signup);
  }
}
